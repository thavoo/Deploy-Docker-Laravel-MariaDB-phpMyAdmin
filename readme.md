Descargar el Instalador con el Siguiente Comando: 'git clone https://gitlab.com/vcandela/Deploy-Docker-Laravel-MariaDB-phpMyAdmin.git Docker'

- Ingresar al Directorio: 'cd Docker'
- Descargar tu Aplicacion Laravel:  'git clone https://github.com/laravel/laravel.git core'
- Ejecutar: 'docker-compose pull'
- Ejecutar: 'docker-compose up -d'
- Ejecutar: 'docker exec -it Docker_Laravel bash'
- Ejecutar: 'composer install'
- Ejecutar: 'cp .env.example .env'
- Ejecutar: 'php artisan key:generate'
- Ejecutar: 'chmod 777 -R storage bootstrap public'
- Ejecutar: 'echo "<?php phpinfo() ?>" >> /var/www/public/infophp.php'

- Configurar el .env con los Siguientes Datos:
	DB_CONNECTION=mysql
	DB_HOST=docker_db
	DB_PORT=3306
	DB_DATABASE=Laravel_DB
	DB_USERNAME=root
	DB_PASSWORD=Laravel_DB
